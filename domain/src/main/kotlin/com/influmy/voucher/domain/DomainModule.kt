package com.influmy.voucher.domain

import com.google.inject.AbstractModule
import com.influmy.voucher.ports.provided.IVoucher
import com.influmy.voucher.ports.provided.IVoucherUrl

class DomainModule : AbstractModule() {
    override fun configure() {
        bind(IVoucher::class.java).to(VoucherDomain::class.java)
        bind(IVoucherUrl::class.java).to(VoucherUrlDomain::class.java)
    }
}
