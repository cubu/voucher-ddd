package com.influmy.voucher.domain

import javax.inject.Inject
import java.util.UUID
import com.influmy.voucher.ports.provided.IVoucher
import com.influmy.voucher.ports.required.IVoucherRepository
import com.influmy.voucher.ports.required.VoucherDTO
import com.influmy.voucher.ports.required.VoucherInfoDTO
import com.influmy.voucher.ports.required.SingleVoucherInfoDTO
import com.influmy.voucher.ports.required.SingleVoucherDTO
import com.influmy.voucher.ports.required.VoucherStatus
import com.influmy.voucher.ports.required.VoucherUnitsLeft
import com.influmy.voucher.ports.required.Influencer

class VoucherDomain @Inject constructor (private val voucherRepository: IVoucherRepository) : IVoucher {
    override suspend fun getAllVouchers(): List<VoucherDTO?> {
        return voucherRepository.getAllVouchers()
    }
    override suspend fun findVoucher(id: UUID): VoucherDTO? {
        return voucherRepository.findVoucher(id)
    }
    override suspend fun nativeFindVoucher(id: UUID): ArrayList<Pair<String, String>>?  {
        return voucherRepository.nativeFindVoucher(id)
    }

    override suspend fun findVoucherByCampaignId(id: UUID): List<VoucherDTO?> {
        return voucherRepository.findVoucherByCampaignId(id)
    }
    override suspend fun findInfoVoucherByCampaignId(id: UUID): VoucherInfoDTO? {
        return voucherRepository.findInfoVoucherByCampaignId(id)
    }
    override suspend fun findInfoVoucherByCampaignIdAndInfluencerId(idCampaign: UUID, idInfluencer: UUID): SingleVoucherInfoDTO? {
        var vouchersFromDb: List<VoucherDTO?> = this.findVoucherByCampaignId(idCampaign);

        val vouchersEnabled = vouchersFromDb.filter { it: VoucherDTO? ->
            (it?.status == VoucherStatus.ENABLED)
        }

        val influencer: Influencer = (vouchersFromDb[0]?.id_influencers as ArrayList<Influencer>).filter{ it: Influencer -> it.id_influencer == idInfluencer.toString() }[0]

        var unitsLeftByInfluencer = arrayOf<VoucherUnitsLeft>()
        val influencersArr = (vouchersFromDb[0]?.id_influencers as ArrayList<Influencer>).filter{ it: Influencer -> it.id_influencer == idInfluencer.toString() }

        influencersArr.forEach { it: Influencer ->

            val id_influencer = it.id_influencer
            val vouchersBought = vouchersFromDb.filter { it: VoucherDTO? ->
                (it?.influencer_referral == id_influencer) && (it?.status == VoucherStatus.BOUGHT)
            }

            val vouchersBlocked = vouchersFromDb.filter { it: VoucherDTO? ->
                ((it?.influencer_referral == id_influencer) && (it?.status == VoucherStatus.BLOCKED))
            }

            unitsLeftByInfluencer += VoucherUnitsLeft(
                    units_left = it.units - vouchersBought.count(),
                    units_bought = vouchersBought.count(),
                    units_blocked = vouchersBlocked.count(),
                    id_influencer = it.id_influencer,
                    total_units = it.units,
                    unlimited = it.unlimited
            )
        }

        return toSingleVoucherInfo(vouchersFromDb[0], unitsLeftByInfluencer[0], vouchersEnabled.count(), influencer)
    }
    override suspend fun blockVoucher(idCampaign: UUID, quantity: Int, token: String, idInfluencer: String): List<VoucherDTO?> {
        return voucherRepository.blockVoucher(idCampaign, quantity, token, idInfluencer)
    }
    override suspend fun unblockVoucher(campaignId: UUID, quantity: Int, token: String): Int {
        return voucherRepository.unblockVoucher(campaignId, quantity, token)
    }
    override suspend fun buyVouchers(token: String?, quantity: Int, idUser: String?, idsToBuy: List<String>): List<VoucherDTO?> {
        return voucherRepository.buyVouchers(token, quantity, idUser, idsToBuy)
    }
    override suspend fun addSingleVoucher(singleVoucher: SingleVoucherDTO): VoucherDTO {
        return voucherRepository.addSingleVoucher(singleVoucher)
    }
    override suspend fun cleanBlocks(): Boolean {
        return voucherRepository.cleanBlocks()
    }
}
