package com.influmy.voucher.domain

import javax.inject.Inject
import com.influmy.voucher.ports.provided.IVoucherUrl
import com.influmy.voucher.ports.required.IVoucherUrlRepository
import com.influmy.voucher.ports.required.VoucherUrlDTO

class VoucherUrlDomain @Inject constructor (private val voucherUrlRepository: IVoucherUrlRepository) : IVoucherUrl {

    override suspend fun findByUrl(url: String): VoucherUrlDTO? {
        return voucherUrlRepository.findByUrl(url)
    }
}
