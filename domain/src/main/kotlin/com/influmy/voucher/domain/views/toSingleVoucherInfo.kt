package com.influmy.voucher.domain

import com.influmy.voucher.ports.required.SingleVoucherInfoDTO
import com.influmy.voucher.ports.required.Influencer
import com.influmy.voucher.ports.required.VoucherUnitsLeft
import com.influmy.voucher.ports.required.VoucherDTO
import org.joda.time.DateTime

public fun toSingleVoucherInfo(voucher: VoucherDTO?, unitsLeftByInfluencer: VoucherUnitsLeft, currentUnitsLeft: Int, influencer: Influencer): SingleVoucherInfoDTO =
                SingleVoucherInfoDTO(
                        id_campaign = voucher?.id_campaign?.toString() as String,
                        name = voucher?.name,
                        description = voucher?.description,
                        short_description = voucher?.short_description,
                        promotion = voucher?.promotion,
                        promotion_description = voucher?.promotion_description,
                        influencer = influencer,
                        id_business = voucher?.id_business,
                        price = voucher?.price,
                        validation_start_date = voucher?.validation_start_date as DateTime,
                        validation_end_date = voucher?.validation_end_date as DateTime,
                        unitsLeftByInfluencer = unitsLeftByInfluencer,
                        currentUnitsLeft = currentUnitsLeft
                )


