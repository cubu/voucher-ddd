import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
//import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val guice_version: String by project



plugins {
    base
    //application
    kotlin("jvm") version "1.3.41"
    id("com.github.johnrengelman.shadow") version "4.0.4"
}



allprojects {

    group = "com.influmy"
    version = "0.0.1"



    apply(plugin = "kotlin")

    repositories {
        mavenLocal()
        jcenter()
        maven { url = uri("https://kotlin.bintray.com/ktor") }
        maven { url = uri("https://plugins.gradle.org/m2/") }
    }

    dependencies {
        compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
        compile("io.ktor:ktor-server-netty:$ktor_version")
        compile("ch.qos.logback:logback-classic:$logback_version")
        compile("io.ktor:ktor-server-core:$ktor_version")
        compile("io.ktor:ktor-freemarker:$ktor_version")
        compile("io.ktor:ktor-jackson:$ktor_version")
        testCompile("io.ktor:ktor-server-tests:$ktor_version")
    }


}

//application {
//    mainClassName = "io.ktor.server.netty.EngineMain"
//}
