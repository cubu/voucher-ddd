//import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

val kotlin_version: String by rootProject
val guice_version: String by rootProject
val exposed_version: String by rootProject

plugins {
    kotlin("jvm")
    //id("com.github.johnrengelman.shadow")
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    //compile(project(":domain"))
    compile("com.google.inject:guice:$guice_version")
    compile ("org.jetbrains.exposed:exposed:$exposed_version")
}

/*tasks {
    "shadowJar"(ShadowJar::class) {
        baseName = "ports"
        classifier = null
        version = null
    }
}*/