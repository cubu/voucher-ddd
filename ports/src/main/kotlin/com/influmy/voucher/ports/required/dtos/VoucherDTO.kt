package com.influmy.voucher.ports.required

import org.joda.time.DateTime
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.DateDeserializers
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import com.influmy.voucher.ports.required.VoucherStatus

data class VoucherDTO(
        val id: String,
        val id_campaign: String,
        val name: String?,
        val description: String?,
        val short_description: String?,
        val promotion: String?,
        val promotion_description: String?,
        val status: VoucherStatus,
        val id_influencers: Any?,
        val id_businesses: Any?,
        val id_business: String?,
        val price: Int?,
        val payment_token: String?,
        @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = DateDeserializers.DateDeserializer::class)
        val validation_start_date: DateTime,
        @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = DateDeserializers.DateDeserializer::class)
        val validation_end_date: DateTime,
        @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = DateDeserializers.DateDeserializer::class)
        val blocked_at: DateTime,
        @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = DateDeserializers.DateDeserializer::class)
        val unblocked_at: DateTime,
        val influencer_fee: Int?,
        val business_fee: Int?,
        val influmy_fee: Int?,
        val user_sold: String?,
        val influencer_referral: String?
)

data class Influencer (
        val id_influencer: String,
        val units: Int,
        val unlimited: Boolean
)

data class Business (
        val id_business: String
)