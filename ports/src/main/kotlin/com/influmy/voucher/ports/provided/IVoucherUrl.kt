package com.influmy.voucher.ports.provided

import java.util.UUID
import com.influmy.voucher.ports.required.VoucherUrlDTO
import com.influmy.voucher.ports.required.SingleVoucherDTO
import com.influmy.voucher.ports.required.VoucherInfoDTO

interface IVoucherUrl {
    suspend fun findByUrl(url: String): VoucherUrlDTO?
}
