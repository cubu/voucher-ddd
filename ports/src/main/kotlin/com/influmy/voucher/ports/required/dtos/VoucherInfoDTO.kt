package com.influmy.voucher.ports.required

import org.joda.time.DateTime
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.DateDeserializers
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer

data class VoucherInfoDTO(
        val id_campaign: String,
        val name: String?,
        val description: String?,
        val short_description: String?,
        val promotion: String?,
        val promotion_description: String?,
        val id_influencers: Any?,
        val id_businesses: Any?,
        val id_business: String?,
        val price: Int?,
        @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = DateDeserializers.DateDeserializer::class)
        val validation_start_date: DateTime,
        @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = DateDeserializers.DateDeserializer::class)
        val validation_end_date: DateTime,
        val unitsLeftByInfluencer: Array<VoucherUnitsLeft>?,
        val currentUnitsLeft: Int
)

class VoucherUnitsLeft(
        val id_influencer: String,
        val units_left: Int,
        val units_bought: Int,
        val units_blocked: Int,
        val total_units: Int,
        val unlimited: Boolean
)