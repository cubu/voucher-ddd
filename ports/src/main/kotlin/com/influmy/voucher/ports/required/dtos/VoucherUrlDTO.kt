package com.influmy.voucher.ports.required

import org.joda.time.DateTime
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.DateDeserializers
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer

data class VoucherUrlDTO(
        val id_influencer: String?,
        val id_campaign: String?,
        val url: String?
)

