package com.influmy.voucher.ports.required

import java.util.UUID

interface IVoucherRepository {
    suspend fun getAllVouchers(): List<VoucherDTO?>
    suspend fun findVoucher(id: UUID): VoucherDTO?
    suspend fun nativeFindVoucher(id: UUID): ArrayList<Pair<String, String>>?
    suspend fun findVoucherByCampaignId(id: UUID): List<VoucherDTO?>
    suspend fun findInfoVoucherByCampaignId(id: UUID): VoucherInfoDTO?
    suspend fun blockVoucher(campaignId: UUID, quantity: Int, token: String, id_influencer: String): List<VoucherDTO?>
    suspend fun unblockVoucher(campaignId: UUID, quantity: Int, token: String): Int
    suspend fun buyVouchers(token: String?, quantity: Int, idUser: String?, idsToBuy: List<String>): List<VoucherDTO?>
    suspend fun addSingleVoucher(newSingleVoucher: SingleVoucherDTO) : VoucherDTO
    suspend fun cleanBlocks(): Boolean
}
