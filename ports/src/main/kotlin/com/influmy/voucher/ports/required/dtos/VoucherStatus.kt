package com.influmy.voucher.ports.required

enum class VoucherStatus {
    ENABLED,
    DELETED,
    BLOCKED,
    BOUGHT
}