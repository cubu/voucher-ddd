package com.influmy.voucher.ports.required


data class BuyVoucherDTO(
    val quantity: Int,
    val token: String?,
    val idUser: String?,
    val idsToBuy: List<String>
)
