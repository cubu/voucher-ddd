package com.influmy.voucher.ports.required

import org.joda.time.DateTime
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.DateDeserializers
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import com.influmy.voucher.ports.required.Influencer

data class SingleVoucherInfoDTO(
    val id_campaign: String,
    val name: String?,
    val description: String?,
    val short_description: String?,
    val promotion: String?,
    val promotion_description: String?,
    val influencer: Influencer,
    val id_business: String?,
    val price: Int?,
    @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = DateDeserializers.DateDeserializer::class)
        val validation_start_date: DateTime,
    @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = DateDeserializers.DateDeserializer::class)
        val validation_end_date: DateTime,
    val unitsLeftByInfluencer: VoucherUnitsLeft?,
    val currentUnitsLeft: Int
)

