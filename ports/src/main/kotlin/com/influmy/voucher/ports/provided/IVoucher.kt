package com.influmy.voucher.ports.provided

import java.util.UUID
import com.influmy.voucher.ports.required.VoucherDTO
import com.influmy.voucher.ports.required.SingleVoucherDTO
import com.influmy.voucher.ports.required.VoucherInfoDTO
import com.influmy.voucher.ports.required.SingleVoucherInfoDTO

interface IVoucher {
    suspend fun getAllVouchers(): List<VoucherDTO?>
    suspend fun findVoucher(id: UUID): VoucherDTO?
    suspend fun nativeFindVoucher(id: UUID): ArrayList<Pair<String, String>>?
    suspend fun findVoucherByCampaignId(id: UUID): List<VoucherDTO?>
    suspend fun findInfoVoucherByCampaignId(id: UUID): VoucherInfoDTO?
    suspend fun findInfoVoucherByCampaignIdAndInfluencerId(idCampaign: UUID, idInfluencer: UUID): SingleVoucherInfoDTO?
    suspend fun blockVoucher(idCampaign: UUID, quantity: Int, token: String, idInfluencer: String): List<VoucherDTO?>
    suspend fun unblockVoucher(campaignId: UUID, quantity: Int, token: String): Int
    suspend fun buyVouchers(token: String?, quantity: Int, idUser: String?, idsToBuy: List<String>): List<VoucherDTO?>
    suspend fun addSingleVoucher(singleVoucher: SingleVoucherDTO): VoucherDTO
    suspend fun cleanBlocks(): Boolean
}
