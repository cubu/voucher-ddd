package com.influmy.voucher.ports.required

import com.influmy.voucher.ports.required.Business


data class SingleVoucherDTO(
    val name: String,
    val price: Int,
    val id_businesses: Array<Business>
)
