package com.influmy.voucher.ports.required


data class BlockVoucherDTO(
    val idCampaign: String,
    val idInfluencer: String,
    val quantity: Int,
    val token: String
)
