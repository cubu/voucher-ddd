package com.influmy.voucher.ports.required


data class UnblockVoucherDTO(
    val idCampaign: String,
    val quantity: Int,
    val token: String
)
