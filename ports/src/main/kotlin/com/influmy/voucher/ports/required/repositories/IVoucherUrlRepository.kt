package com.influmy.voucher.ports.required


interface IVoucherUrlRepository {
    suspend fun findByUrl(url: String): VoucherUrlDTO?
}
