pluginManagement {
    repositories {
        mavenCentral()
        maven { url = uri("https://kotlin.bintray.com/kotlin-eap") }
        maven { url = uri("https://plugins.gradle.org/m2/") }
    }
}

rootProject.name = "voucher"
rootProject.buildFileName = "build.gradle.kts"
include("ports", "domain", "adapters", "app")