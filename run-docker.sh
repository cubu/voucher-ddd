./gradlew build
set -a
source ./services.envar
docker build -t influmy-voucher .
docker run --network="development" -p ${PORT}:${PORT} --env-file services.envar -it --rm influmy-voucher