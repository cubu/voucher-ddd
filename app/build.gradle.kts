import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

val ktor_version: String by rootProject
val kotlin_version: String by rootProject
val logback_version: String by rootProject
val guice_version: String by rootProject
val exposed_version: String by rootProject

application {
    mainClassName = "io.ktor.server.netty.EngineMain"
}

plugins {
    application
    kotlin("jvm")
    id("com.github.johnrengelman.shadow")
}

dependencies {
    compile(project(":ports"))
    compile(project(":adapters"))
    compile(project(":domain"))
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    compile("io.ktor:ktor-server-netty:$ktor_version")
    compile("ch.qos.logback:logback-classic:$logback_version")
    compile("io.ktor:ktor-metrics:$ktor_version")
    compile("io.ktor:ktor-server-core:$ktor_version")
    compile("io.ktor:ktor-server-host-common:$ktor_version")
    compile("io.ktor:ktor-auth:$ktor_version")
    compile("io.ktor:ktor-auth-jwt:$ktor_version")
    compile("io.ktor:ktor-gson:$ktor_version")
    compile("io.ktor:ktor-locations:$ktor_version")
    compile("com.google.inject:guice:$guice_version")
    testCompile("io.ktor:ktor-server-tests:$ktor_version")
    testCompile("io.kotlintest:kotlintest-assertions:3.1.10")
    testCompile("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.1.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.1.0")

    compile ("io.ktor:ktor-jackson:$ktor_version")
    compile ("org.jetbrains.exposed:exposed:$exposed_version")
    compile ("com.zaxxer:HikariCP:3.3.1")
    compile ("org.postgresql:postgresql:42.2.2")

    implementation("io.ktor:ktor-freemarker:$ktor_version")
    implementation("io.ktor:ktor-auth:$ktor_version")

}

/*tasks {
    "shadowJar"(ShadowJar::class) {
        baseName = "app"
        classifier = null
        version = null
    }
}*/