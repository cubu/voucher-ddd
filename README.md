# Influmy Voucher Rest API 

## Setup

Assuming you have postgres instance exposed at port 5432 with a 'influmy_voucher' database and a table voucher and a voucher_url for testing purposes only:
```
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
DROP TABLE IF EXISTS voucher;

DROP TYPE IF EXISTS voucher_status;
CREATE TYPE voucher_status AS ENUM ('ENABLED', 'DELETED', 'BLOCKED', 'BOUGHT');

CREATE TABLE public.voucher
(
    id character(36) COLLATE pg_catalog."default" NOT NULL,
    id_campaign character(36) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    description character varying(1000) COLLATE pg_catalog."default",
    short_description character varying(500) COLLATE pg_catalog."default",
    promotion character varying(510) COLLATE pg_catalog."default",
    promotion_description character varying(500) COLLATE pg_catalog."default",
    status voucher_status NOT NULL DEFAULT 'ENABLED'::voucher_status,
    id_influencers jsonb[],
    id_businesses jsonb[],
    id_business char(36),
    price integer,
    payment_token character varying(500) COLLATE pg_catalog."default",
    created_by character varying(255) COLLATE pg_catalog."default",
    updated_by character varying(255) COLLATE pg_catalog."default",
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    updated_at timestamp without time zone NOT NULL DEFAULT now(),
    validation_start_date timestamp without time zone NOT NULL,
    validation_end_date timestamp without time zone NOT NULL,
    blocked_at timestamp without time zone,
    unblocked_at timestamp without time zone,
    influencer_fee integer,
    business_fee integer,
    influmy_fee integer,
    user_sold character(36) COLLATE pg_catalog."default",
    influencer_referral character(36) COLLATE pg_catalog."default",
    CONSTRAINT voucher_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE INDEX ON voucher (id_campaign);
CREATE INDEX ON voucher (payment_token);
CREATE INDEX ON voucher (status);

DROP TABLE public.voucher_url;

CREATE TABLE public.voucher_url
(
    id_influencer character(36) COLLATE pg_catalog."default" NOT NULL,
    id_campaign character(36) COLLATE pg_catalog."default" NOT NULL,
    url character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT voucher_url_pkey PRIMARY KEY (url)
)
WITH (
    OIDS = FALSE
);

CREATE INDEX ON voucher_url ((lower(url)));
CREATE INDEX ON voucher_url (id_campaign);
CREATE INDEX ON voucher_url (id_influencer);
```

## Running locally

Change services.envar to the correct host to access your postgres instance and then run:
```
sudo bash run-local.sh
```

## Running in Docker
Change services.envar to the correct host to access your postgres, noting that the container will be in network development by default, and then run:
```
sudo bash run-docker.sh
```


Bonus query:

```
INSERT INTO public.voucher(
	id, id_campaign, name, description, short_description, promotion, promotion_description, status, id_influencers, id_businesses, id_business, price, 
	payment_token, created_by, updated_by, created_at, updated_at, validation_start_date, validation_end_date, blocked_at, unblocked_at, influencer_fee,
	business_fee, influmy_fee, user_sold, influencer_referral
	)
	VALUES ('6a084207-ac45-4e67-999c-be13c7f7e3f6', '6ce6b101-350e-40e8-a561-2b54103db9eb', 'Vegano Bono', 'El bono vegano', 'l bn vgn', '2x1', 'en tu menú', 'ENABLED',
			(ARRAY['{"id_influencer":"8534e55b-9022-466a-9284-976305427db0","units":"5","unlimited":"false"}','{"id_influencer":"e32e4ce5-0f42-4ae5-a5a2-aba873e38350","units":"0","unlimited":"true"}']::jsonb[]),
			(ARRAY['{"id_business":"2b787d2a-9348-4633-82c0-5532cc054410"}']::jsonb[]),
			'c5e27674-f91a-42b9-ad8c-4bd111e0fd09',
			300, '', 'christian', 'christian', now(), now(), now(), now(), now(), now(), 100, 100, 100, '28632f17-6af6-411f-b1bf-610c31b8b372', 'e16ecf1b-f988-490f-9700-8efa029927dc');
```

```INSERT INTO public.voucher_url(
   	id_influencer, id_campaign, url)
   	VALUES ('8534e55b-9022-466a-9284-976305427db0', '6ce6b101-350e-40e8-a561-2b54103db9eb', 'daso-golafres');
```

