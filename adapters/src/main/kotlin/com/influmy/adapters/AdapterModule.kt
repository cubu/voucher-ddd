package com.influmy.adapters

import com.google.inject.AbstractModule
import com.influmy.adapters.db.VoucherRepository
import com.influmy.adapters.db.VoucherUrlRepository
import com.influmy.adapters.routes.VoucherRoutes
import com.influmy.adapters.routes.CampaignRoutes
import com.influmy.voucher.ports.required.IVoucherRepository
import com.influmy.voucher.ports.required.IVoucherUrlRepository
import io.ktor.util.KtorExperimentalAPI

class AdapterModule : AbstractModule() {

    @KtorExperimentalAPI
    override fun configure() {
        // N.B. - order matters, application config needs to be loaded first
        bind(ApplicationConfig::class.java).asEagerSingleton()
        bind(VoucherRoutes::class.java).asEagerSingleton()
        bind(CampaignRoutes::class.java).asEagerSingleton()
        bind(IVoucherRepository::class.java).to(VoucherRepository::class.java)
        bind(IVoucherUrlRepository::class.java).to(VoucherUrlRepository::class.java)
    }
}
