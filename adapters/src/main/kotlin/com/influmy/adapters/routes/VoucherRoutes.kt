package com.influmy.adapters.routes

import com.google.inject.Inject
import com.influmy.voucher.ports.provided.IVoucher
import com.influmy.voucher.ports.provided.IVoucherUrl
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.routing.*
import java.util.*
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import com.influmy.voucher.ports.required.SingleVoucherDTO
import com.influmy.voucher.ports.required.BlockVoucherDTO
import com.influmy.voucher.ports.required.UnblockVoucherDTO
import com.influmy.voucher.ports.required.BuyVoucherDTO

class VoucherRoutes @Inject constructor(application: Application, voucher: IVoucher, voucherUrl: IVoucherUrl) {
    init {
        application.routing {


            route("/vouchers") {

                get("/") {
                    val vouchers = voucher.getAllVouchers()

                    if (vouchers.count() == 0) call.respond(HttpStatusCode.NotFound)
                    else call.respond(vouchers)
                }

                get("/ping") {
                    call.respondText("Pong: API Voucher running...", contentType = ContentType.Text.Plain)
                }

                get("/cleanBlocks") {
                    voucher.cleanBlocks()
                    call.respond(HttpStatusCode.OK)
                }

                get("/url/{url}") {
                    val url = call.parameters["url"]
                    url ?: throw IllegalStateException("Must provide url")
                    val voucherUrlRes = voucherUrl.findByUrl(url)
                    if (voucherUrlRes == null) call.respond(HttpStatusCode.NoContent)
                    else call.respond(voucherUrlRes)
                }

                get("/{id}") {
                    val pid = call.parameters["id"];
                    val id = UUID.fromString(pid) ?: throw IllegalStateException("Must provide id");
                    val voucherRes = voucher.findVoucher(id)

                    if (voucherRes == null) call.respond(HttpStatusCode.NoContent)
                    else call.respond(voucherRes)
                }

                get("/native/{id}") {
                    val pid = call.parameters["id"];
                    val id = UUID.fromString(pid) ?: throw IllegalStateException("Must provide id");
                    val voucherRes = voucher.nativeFindVoucher(id)

                    if (voucherRes == null) call.respond(HttpStatusCode.NoContent)
                    else call.respond(voucherRes)
                }

                get("/getByCampaignId/{id}") {
                    val pid = call.parameters["id"];
                    val id = UUID.fromString(pid) ?: throw IllegalStateException("Must provide id");
                    val vouchers = voucher.findVoucherByCampaignId(id)

                    if (vouchers.count() == 0) call.respond(HttpStatusCode.NoContent)
                    else call.respond(vouchers)
                }

                get("/getInfoByCampaignId/{id}") {
                    val pid = call.parameters["id"];
                    val id = UUID.fromString(pid) ?: throw IllegalStateException("Must provide id");
                    val voucherInfo = voucher.findInfoVoucherByCampaignId(id)

                    if (voucherInfo == null) call.respond(HttpStatusCode.NoContent)
                    else call.respond(voucherInfo)
                }

                post("/singlevoucher") {
                    val voucherReceived = call.receive<SingleVoucherDTO>()
                    val voucherRes = voucher.addSingleVoucher(voucherReceived)
                    call.respond(HttpStatusCode.Created, voucherRes)
                }

                post("/block") {
                    val voucherReceived = call.receive<BlockVoucherDTO>()
                    val vouchers = voucher.blockVoucher(UUID.fromString(voucherReceived.idCampaign), voucherReceived.quantity, voucherReceived.token, voucherReceived.idInfluencer)
                    call.respond(HttpStatusCode.OK, vouchers)
                }

                post("/unblock") {
                    val voucherReceived = call.receive<UnblockVoucherDTO>()
                    val vouchersUnblocked = voucher.unblockVoucher(UUID.fromString(voucherReceived.idCampaign), voucherReceived.quantity, voucherReceived.token)
                    call.respond(HttpStatusCode.OK, vouchersUnblocked)
                }

                post("/buy") {
                    val voucherReceived = call.receive<BuyVoucherDTO>()
                    val vouchers = voucher.buyVouchers(voucherReceived.token, voucherReceived.quantity, voucherReceived.idUser, voucherReceived.idsToBuy)
                    call.respond(HttpStatusCode.OK, vouchers)
                }
            }
        }
    }
}
