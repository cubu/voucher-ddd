package com.influmy.adapters.routes

import com.google.inject.Inject
import com.influmy.voucher.ports.provided.IVoucher
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing

import io.ktor.http.HttpStatusCode
import java.util.*

class CampaignRoutes @Inject constructor(application: Application, voucher: IVoucher) {
    init {
        application.routing {

            route("/campaigns") {

                get("/ping") {
                    call.respondText("Pong: API Voucher running...", contentType = ContentType.Text.Plain)
                }

                get("/{idCampaign}/influencers/{idInfluencer}") {
                    val pidCampaign = call.parameters["idCampaign"];
                    val pidInfluencer = call.parameters["idInfluencer"];
                    val idCampaign = UUID.fromString(pidCampaign) ?: throw IllegalStateException("Must provide id cmapign");
                    val idInfluencer = UUID.fromString(pidInfluencer) ?: throw IllegalStateException("Must provide id influencer");
                    val voucherInfo = voucher.findInfoVoucherByCampaignIdAndInfluencerId(idCampaign, idInfluencer)

                    if (voucherInfo == null) call.respond(HttpStatusCode.NotFound)
                    else call.respond(voucherInfo)
                }
            }
        }
    }
}
