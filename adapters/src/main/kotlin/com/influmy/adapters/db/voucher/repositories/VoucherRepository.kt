package com.influmy.adapters.db

import org.jetbrains.exposed.sql.*
import java.util.UUID
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.joda.time.DateTime
import com.influmy.adapters.db.DatabaseService.dbQuery
import com.influmy.voucher.ports.required.IVoucherRepository
import com.influmy.voucher.ports.required.VoucherDTO
import com.influmy.voucher.ports.required.Influencer
import com.influmy.voucher.ports.required.VoucherInfoDTO
import com.influmy.voucher.ports.required.SingleVoucherInfoDTO
import com.influmy.voucher.ports.required.VoucherUnitsLeft
import com.influmy.voucher.ports.required.SingleVoucherDTO
import com.influmy.voucher.ports.required.Business
import com.influmy.voucher.ports.required.VoucherStatus


class VoucherRepository : IVoucherRepository {

    override suspend fun findVoucher(id: UUID): VoucherDTO? = dbQuery {
        voucher.select {
            (voucher.id eq id.toString())
        }.mapNotNull { toVoucher(it) }
                .singleOrNull()
    }

    override suspend fun nativeFindVoucher(id: UUID): ArrayList<Pair<String, String>>? = dbQuery {
        TransactionManager.current().exec("select id_influencers from voucher where id='$id'") { rs ->
            val result = arrayListOf<Pair<String, String>>()
            while (rs.next()) {
                result += rs.getString("id_influencers") to rs.getString("id_influencers")
            }
            println(result)
            result
        }
    }

    override suspend fun findVoucherByCampaignId(id: UUID): List<VoucherDTO?> = dbQuery {
        voucher.select {
            (voucher.id_campaign eq id.toString())
        }.mapNotNull { toVoucher(it) }
    }

    override suspend fun findInfoVoucherByCampaignId(id: UUID): VoucherInfoDTO? {
        val vouchersFromDb: List<VoucherDTO?> = dbQuery {
            voucher.select {
                (voucher.id_campaign eq id.toString())
            }.map { toVoucher(it) }
        }

        val vouchersEnabled = vouchersFromDb.filter { it: VoucherDTO? ->
            (it?.status == VoucherStatus.ENABLED)
        }

        var unitsLeftByInfluencer = arrayOf<VoucherUnitsLeft>()
        val influencersArr = vouchersFromDb[0]?.id_influencers as ArrayList<Influencer>

        influencersArr.forEach { it: Influencer ->

            val idInfluencer = it.id_influencer
            val vouchersBought = vouchersFromDb.filter { it: VoucherDTO? ->
                (it?.influencer_referral == idInfluencer) && (it?.status == VoucherStatus.BOUGHT)
            }
            val vouchersBlocked = vouchersFromDb.filter { it: VoucherDTO? ->
                ((it?.influencer_referral == idInfluencer) && (it?.status == VoucherStatus.BLOCKED))
            }

            unitsLeftByInfluencer += VoucherUnitsLeft(
                    units_left = it.units - vouchersBought.count(),
                    units_bought = vouchersBought.count(),
                    units_blocked = vouchersBlocked.count(),
                    id_influencer = it.id_influencer,
                    total_units = it.units,
                    unlimited = it.unlimited
            )
        }
        return toVoucherInfo(vouchersFromDb[0], unitsLeftByInfluencer, vouchersEnabled.count())
    }

    override suspend fun addSingleVoucher(singleVoucher: SingleVoucherDTO): VoucherDTO {
        var id1 = UUID.randomUUID()
        var campaign_id1 = UUID.randomUUID()
        var business = Business(id_business = UUID.randomUUID().toString())
        var businessArr = arrayOf(Business(id_business = UUID.randomUUID().toString()))
        dbQuery {
            voucher.insert{
                it[id] = id1.toString()
                it[id_campaign] = campaign_id1.toString()
                it[name] = singleVoucher.name
                it[price] = singleVoucher.price
                it[id_businesses] = singleVoucher.id_businesses
            } get voucher.id
        }

        return findVoucher(id1)!!
    }

    override suspend fun blockVoucher(idCampaign: UUID, quantity: Int, token: String, idInfluencer: String): List<VoucherDTO?> {
        var idsToReturn = listOf<String>();
        if(quantity < 10) {
            dbQuery {
                val dbVouchersToBlock = voucher.select {
                    ((voucher.id_campaign eq idCampaign.toString()) and ((voucher.payment_token.isNull()) or (voucher.payment_token eq "")))
                }.mapNotNull { toVoucher(it) }

                if(dbVouchersToBlock.count() >= quantity) {
                    val actualVouchersToBlock = dbVouchersToBlock.slice(IntRange(0, quantity - 1))
                    actualVouchersToBlock.forEach {
                        val updRes = voucher.update({ voucher.id eq it.id }) {
                            it[payment_token] = token
                            it[influencer_referral] = idInfluencer
                            it[status] = VoucherStatus.BLOCKED
                            it[blocked_at] = DateTime.now()
                        }

                        if(updRes == 1) {
                            idsToReturn += it.id
                        } else if (updRes > 1) {
                            //oh shit unblock
                        }
                    }
                }

            }
        }

        return idsToReturn.map { findVoucher(UUID.fromString(it)) }
    }

    override suspend fun unblockVoucher(campaignId: UUID, quantity: Int, token: String): Int {
        var vouchersUnblocked = 0;
        dbQuery {
            val dbVouchersToUnBlock = voucher.select {
                ((voucher.id_campaign eq campaignId.toString())
                        and (voucher.payment_token eq token)
                        and (voucher.status eq VoucherStatus.BLOCKED))
            }.mapNotNull { toVoucher(it) }

            if(dbVouchersToUnBlock.count() > 0 && dbVouchersToUnBlock.count() >= quantity) {
                dbVouchersToUnBlock.take(quantity).forEach {
                    val resUpd = voucher.update({ voucher.id eq it.id }) {
                        it[payment_token] = ""
                        it[influencer_referral] = ""
                        it[status] = VoucherStatus.ENABLED
                        it[unblocked_at] = DateTime.now()
                    }
                    if (resUpd == 1) {
                        vouchersUnblocked++
                    }
                }
            }

        }
        return vouchersUnblocked;
    }

    override suspend fun buyVouchers(token: String?, quantity: Int, idUser: String?, idsToBuy: List<String>): List<VoucherDTO?> {

        dbQuery {
            if(idsToBuy.count() == quantity) {
                idsToBuy.forEach {
                    voucher.update({ (voucher.id eq it.toString()) and (voucher.status eq VoucherStatus.BLOCKED) }) {
                        it[user_sold] = idUser.toString()
                        it[status] = VoucherStatus.BOUGHT
                    }
                }
            }
        }

        return idsToBuy.map { findVoucher(UUID.fromString(it)) }
    }

    override suspend fun getAllVouchers(): List<VoucherDTO?> = dbQuery {
        voucher.selectAll().map { toVoucher(it) }
    }

    override suspend fun cleanBlocks(): Boolean {
        val res = dbQuery {
            val dbVouchersToClean = voucher.select {
                ((voucher.status eq VoucherStatus.BLOCKED) and (voucher.payment_token.isNotNull()))
            }.mapNotNull { toVoucher(it) }

            val actualDbVouchersToClean = dbVouchersToClean.filter{ v -> DateTime.now() > v.blocked_at.plusMinutes(10) }

            actualDbVouchersToClean.forEach {

                voucher.update({ voucher.id eq it.id }) {
                    it[payment_token] = ""
                    it[status] = VoucherStatus.ENABLED
                    it[unblocked_at] = DateTime.now()
                }
            }
        }

        return true;
    }

    private fun toVoucher(row: ResultRow): VoucherDTO =
        VoucherDTO(
            id = row[voucher.id].toString(),
            id_campaign = row[voucher.id_campaign]?.toString(),
            name = row[voucher.name],
            description = row[voucher.description],
            short_description = row[voucher.short_description],
            promotion = row[voucher.promotion],
            promotion_description = row[voucher.promotion_description],
            status = row[voucher.status],
            id_influencers = row[voucher.id_influencers],
            id_businesses = row[voucher.id_businesses],
            id_business = row[voucher.id_business],
            price = row[voucher.price],
            payment_token = row[voucher.payment_token],
            validation_start_date = row[voucher.validation_start_date],
            validation_end_date = row[voucher.validation_end_date],
            blocked_at = row[voucher.blocked_at],
            unblocked_at = row[voucher.unblocked_at],
            influencer_fee = row[voucher.influencer_fee],
            business_fee = row[voucher.business_fee],
            influmy_fee = row[voucher.influmy_fee],
            user_sold = row[voucher.user_sold],
            influencer_referral = row[voucher.influencer_referral]
        )

    private fun toVoucherInfo(row: ResultRow, unitsLeftByInfluencer: Array<VoucherUnitsLeft>): VoucherInfoDTO =
        VoucherInfoDTO(
            id_campaign = row[voucher.id_campaign]?.toString(),
            name = row[voucher.name],
            description = row[voucher.description],
            short_description = row[voucher.short_description],
            promotion = row[voucher.promotion],
            promotion_description = row[voucher.promotion_description],
            id_influencers = row[voucher.id_influencers],
            id_businesses = row[voucher.id_businesses],
            id_business = row[voucher.id_business],
            price = row[voucher.price],
            validation_start_date = row[voucher.validation_start_date],
            validation_end_date = row[voucher.validation_end_date],
            unitsLeftByInfluencer = unitsLeftByInfluencer,
            currentUnitsLeft = 0
        )

    private fun toVoucherInfo(voucher: VoucherDTO?, unitsLeftByInfluencer: Array<VoucherUnitsLeft>, currentUnitsLeft: Int): VoucherInfoDTO =
            VoucherInfoDTO(
                    id_campaign = voucher?.id_campaign?.toString() as String,
                    name = voucher?.name,
                    description = voucher?.description,
                    short_description = voucher?.short_description,
                    promotion = voucher?.promotion,
                    promotion_description = voucher?.promotion_description,
                    id_influencers = voucher?.id_influencers,
                    id_businesses = voucher?.id_businesses,
                    id_business = voucher?.id_business,
                    price = voucher?.price,
                    validation_start_date = voucher?.validation_start_date as DateTime,
                    validation_end_date = voucher?.validation_end_date as DateTime,
                    unitsLeftByInfluencer = unitsLeftByInfluencer,
                    currentUnitsLeft = currentUnitsLeft
            )



    private fun toInfluencer(influencer: Influencer): Influencer =
        Influencer(
            id_influencer = influencer.id_influencer,
            units = influencer.units,
            unlimited = influencer.unlimited
        )

}
