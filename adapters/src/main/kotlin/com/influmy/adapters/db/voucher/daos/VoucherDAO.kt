package com.influmy.adapters.db

import com.fasterxml.jackson.module.kotlin.*
import org.jetbrains.exposed.sql.Table
import com.influmy.voucher.ports.required.VoucherStatus
import com.influmy.voucher.ports.required.Business
import com.influmy.voucher.ports.required.Influencer
import org.postgresql.util.PGobject

val JSONMapper = jacksonObjectMapper()

object voucher : Table() {
    val id = varchar("id", 36).primaryKey()
    val id_campaign = varchar("id_campaign", 36)
    val name = varchar("name", 255)
    val description = varchar("description", 1000)
    val short_description = varchar("short_description", 500)
    val promotion = varchar("promotion", 510)
    val promotion_description = varchar("promotion_description", 255)
    val status = customEnumeration("status", "voucher_status",{ value ->
        when (value) {
            is PGobject -> VoucherStatus.valueOf(value.value)
            is String -> VoucherStatus.valueOf(value)
            else -> error("Can't convert Voucher Status")
        }
    }, { PGEnum("voucher_status", it) })
    val id_influencers = jsonbArr("id_influencers", Influencer::class.java, JSONMapper)
    val id_businesses = jsonbArr("id_businesses", Business::class.java, JSONMapper)
    val id_business = varchar("id_business", 36)
    val price = integer("price")
    val payment_token = varchar("payment_token", 500)
    val created_by = varchar("created_by", 255)
    val updated_by = varchar("updated_by", 255)
    val created_at = date("created_at")
    val updated_at = date("updated_at")
    val validation_start_date = date("validation_start_date")
    val validation_end_date = date("validation_end_date")
    val blocked_at = datetime("blocked_at")
    val unblocked_at = datetime("unblocked_at")
    val influencer_fee = integer("influencer_fee")
    val business_fee = integer("business_fee")
    val influmy_fee = integer("influmy_fee")
    val user_sold = varchar("user_sold", 36)
    val influencer_referral = varchar("influencer_referral", 36)
}



