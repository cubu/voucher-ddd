package com.influmy.adapters.db

import org.jetbrains.exposed.sql.Table


object voucher_url : Table() {
    val id_influencer = varchar("id_influencer", 36)
    val id_campaign = varchar("id_campaign", 36)
    val url = varchar("url", 255).primaryKey()
}