package com.influmy.adapters.db

import com.fasterxml.jackson.databind.ObjectMapper
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Table
import org.postgresql.jdbc.PgArray
import java.sql.PreparedStatement

/**
 * Created by quangio.
 */

fun <T : Any> Table.jsonbArr(name: String, klass: Class<T>, jsonMapper: ObjectMapper): Column<Array<T>>
        = registerColumn(name, JsonArr(klass, jsonMapper))


private class JsonArr<out T : Any>(private val klass: Class<T>, private val jsonMapper: ObjectMapper) : ColumnType() {
    override fun sqlType() = "jsonb"

    override fun valueFromDB(value: Any): List<Any> {
        return if (value is PgArray) {
            val array = value.array
            if (array is Array<*>) {
                array.map {
                    jsonMapper.readValue(it.toString(), klass)
                }
            }
            else {
                throw Exception("Values returned from database if not of type kotlin Array<*> ")
            }
        }
        else throw Exception("Values returned from database if not of type PgArray")
    }
    /*override fun notNullValueToDB(value: Any): Any {
        throw java.lang.UnsupportedOperationException("Please use the helper method to insert in db")
    }

    override fun nonNullValueToString(value: Any): String {
        return "'${notNullValueToDB(value)}'"
    }*/

    override fun valueToString(value: Any?): String = when (value) {
        null -> {
            if (!nullable) error("NULL in non-nullable column")
            "NULL"
        }

        is Iterable<*> -> {
            "'{${value.joinToString()}}'"
        }

        else -> {
            nonNullValueToString(value)
        }
    }

    override fun setParameter(stmt: PreparedStatement, index: Int, value: Any?) {
        if (value is Array<*>) {
            println("hola")
            var new = jsonMapper.writeValueAsString(value)
            var newValue = value.map{jsonMapper.writeValueAsString(it)}.toTypedArray()
            println(new)
            //stmt.setArray(index, stmt.connection.createArrayOf("jsonb", newValue))
            //val jsonObject = PGobject()
            //jsonObject.setType("jsonb[]")
            //jsonObject.setValue(newValue)
            //val jsonArray = PGarray()
            stmt.setArray(index, stmt.connection.createArrayOf("jsonb", newValue))
            //stmt.setObject(index, jsonObject)
        } else {
            println("hola2")
            super.setParameter(stmt, index, value)
        }
    }
}



