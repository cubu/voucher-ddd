package com.influmy.adapters.db

import org.jetbrains.exposed.sql.*
import com.influmy.adapters.db.DatabaseService.dbQuery
import com.influmy.voucher.ports.required.IVoucherUrlRepository
import com.influmy.voucher.ports.required.VoucherUrlDTO

class VoucherUrlRepository : IVoucherUrlRepository {


    override suspend fun findByUrl(url: String): VoucherUrlDTO? = dbQuery {
        voucher_url.select {
            (voucher_url.url eq url.toString())
        }.mapNotNull { toVoucherUrl(it) }
                .singleOrNull()
    }



    private fun toVoucherUrl(row: ResultRow): VoucherUrlDTO =
        VoucherUrlDTO(
                id_influencer = row[voucher_url.id_influencer].toString(),
                id_campaign = row[voucher_url.id_campaign]?.toString(),
                url = row[voucher_url.url]
        )

}
