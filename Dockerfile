ARG VERSION=8

FROM openjdk:${VERSION}-jdk as BUILD

COPY . /src
WORKDIR /src
RUN ./gradlew --no-daemon shadowJar

FROM openjdk:${VERSION}-jre

COPY --from=BUILD /src/app/build/libs/app-0.0.1-all.jar /bin/runner/run.jar
WORKDIR /bin/runner

EXPOSE ${PORT}

CMD ["java","-jar","run.jar"]